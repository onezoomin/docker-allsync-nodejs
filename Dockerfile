FROM registry.gitlab.com/onezoomin/docker-allsync

RUN apk update && apk add --no-cache nodejs npm

CMD ["/bin/bash"]
